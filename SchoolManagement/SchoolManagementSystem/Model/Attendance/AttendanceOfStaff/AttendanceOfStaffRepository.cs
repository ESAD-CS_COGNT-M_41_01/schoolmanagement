﻿using SchoolManagementSystem.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace SchoolManagementSystem.Model
{
    public class AttendanceOfStaffRepository : IAttendanceOfStaffReopsitory
    {
        ApplicationDbContext _context;
        public AttendanceOfStaffRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public IEnumerable<object> GetStaffByNumber()
        {
            var staff = _context.Staff.Select(a => new { a.Id, a.FirstName }).ToList();
            if (staff != null)
            {
                return staff;
            }
            return null;
        }

        public string GetStaffFingerData(int id)
        {
            if (StaffExists(id))
            {
                var finger = _context.Staff.Where(s => s.Id == id).Select(a => a.FingerData).FirstOrDefault();
                return finger;
            }
            return null;
        }

        public int GetStaffStatus(int id)
        {
            int outId = 0;
            string countEnrollment = _context.Staff.Where(ta => ta.Id == id).Select(a => a.FingerData).FirstOrDefault();
            if (countEnrollment == null)
            {
                outId = -1;
            }
            else
            {
                int countAttendance = _context.AttendanceOfStaff.Where(ta => ta.StaffId == id && ta.Date == DateTime.UtcNow.AddHours(6).Date).Count();
                if (countAttendance > 0)
                {
                    outId = _context.AttendanceOfStaff.Single(ta => ta.StaffId == id && ta.Date == DateTime.UtcNow.AddHours(6).Date).Id;
                }
            }
            return outId;
        }


        public int registerStaffFinger(int id, string fingerdt)
        {
            if (StaffExists(id))
            {
                var staff = _context.Staff.Where(s => s.Id == id).FirstOrDefault();
                staff.FingerData = fingerdt;
                _context.Staff.Update(staff);
                _context.SaveChanges();
                return 1;
            }
            return 0;
        }

        public int SaveAttendance(int staffId)
        {
            TimeSpan time = DateTime.UtcNow.AddHours(6).TimeOfDay;
            AttendanceOfStaff attendanceOfStaff = _context.AttendanceOfStaff.Where(ta => ta.StaffId == staffId && ta.Date == DateTime.UtcNow.AddHours(6).Date).FirstOrDefault();

            if (attendanceOfStaff == null)
            {
                AttendanceOfStaff NewAttendanceOfStaff = new AttendanceOfStaff { StaffId = staffId, Date = DateTime.UtcNow.AddHours(6).Date, InTime = time, OutTime = time };
                _context.AttendanceOfStaff.Add(NewAttendanceOfStaff);
                _context.SaveChanges();
                return 1;
            }
            return 0;
        }

        public void UpdateForOutTime(int id)
        {
            AttendanceOfStaff attendanceOfStaff = _context.AttendanceOfStaff.Find(id);

            attendanceOfStaff.OutTime = DateTime.UtcNow.AddHours(6).TimeOfDay;
            _context.AttendanceOfStaff.Update(attendanceOfStaff);
            _context.SaveChanges();
        }

        private bool StaffExists(int id)
        {
            return _context.Staff.Any(e => e.Id == id);
        }
    }
}
